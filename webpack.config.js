var path = require('path');
var fs = require('fs');
var HtmlWebpackPlugin = require('html-webpack-plugin');

// Look for .html files
var htmlFiles = [];
var directories = ['src'];
while (directories.length > 0) {
  var directory = directories.pop();
  var dirContents = fs.readdirSync(directory)
       .map(file => path.join(directory, file));

  htmlFiles.push(...dirContents.filter(file => file.endsWith('.html')));
  directories.push(...dirContents.filter(file => fs.statSync(file).isDirectory()));
}

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: 'assets/images/[name]-[hash][ext]'
  },

  devtool: 'eval-source-map',
  devServer: {
  },
    
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ]
      },

      {
        test: /\.(png|jpg|webp)$/i,
        use: [
          {
            loader: 'responsive-loader',
            options: {
              // adapter: require('responsive-loader/sharp'),
              sizes: [320, 640, 960, 1200, 1800, 2400],
              name: '[name]-[width]-[hash].[ext]',
              outputPath: 'assets/images/',
              }
          }
        ],
        // type: 'javascript/auto',
      },
      {
        test: /\.html$/i,
        use: [
          'html-loader'
        ]
      },
    ]
  },

  plugins: [
    // Build a new plugin instance for each .html file found
    ...htmlFiles.map(htmlFile =>
      new HtmlWebpackPlugin({
        template: htmlFile,
        filename: htmlFile.replace(path.normalize('src/'), ''),
        // semble ajouter le css
        inject: 'body'
      })
    )
  ]
};
